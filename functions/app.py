import json
import boto3
import requests
import os
import service      # .dependencies.python.service

BUCKET_NAME = os.environ['BUCKET_NAME']
OUTPUT_DIR = os.environ['OUTPUT_DIR']

s3_client = boto3.client('s3')

def lambda_handler(event, context):
    api_response = service.get_api_response()

    s3_client.put_object(
        Body=json.dumps(api_response).encode(),
        Bucket=BUCKET_NAME,
        Key=f'{OUTPUT_DIR}/test.txt'
    )

    print(api_response)
    print('WORKING!')
