# AWS Serverless Cronjob

---

## Pre requisites

- Python 3.8
- [AWS CLI](https://aws.amazon.com/cli/)
  - Configured for deployment
- [AWS SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)
  - Docker - required to run serverless functions locally
  - Python - required to install SAM CLI
    - ```pip install aws-sam-cli```
- Setup environment variables in your [.env file](./src/.env)
  - Create **.env** file from **.env.example**
    - ```cp src/.env.example src/.env```
- Modify [samconfig.toml](./src/samconfig.toml) (optional)

---

## Deployment

### Export environment variables

```sh
source .env
```

### Create bucket

```sh
aws s3 mb s3://$BUCKET_NAME
```

### Build SAM deployment package

```sh
sam build
```

### Deploy package to S3

```sh
sam deploy --s3-bucket $BUCKET_NAME --parameter-overrides BucketName=$BUCKET_NAME
```

---

## Local lambda invocation

To locally invoke lambda the correct env vars must be avilable to the sam context.  
At the time of developing this solution; sam does not officially support **dotenv** based environment variables for lambda invocations - github issue [here](https://github.com/awslabs/aws-sam-cli/issues/1355).  
Hence exposing environment variables requires us to duplicate all variables present in [.env](.env) file as an **env.json** file.  
Example (using single env var):

```sh
echo \{ \"ScheduledEventLogger\": \{ \"BUCKET_NAME\": \"${BUCKET_NAME}\" \} \} > env.json
```

Modifying code required rebuilding for invocations:

```sh
sam build && sam local invoke -t .aws-sam/build/template.yaml --env-vars .aws-sam/build/ScheduledEventLogger/env.json
```

---

## Cleanup

To remove all deployed resources, we just need to empty the S3 bucket then delete the cloudformation stack

```sh
aws s3 rb s3://$BUCKET_NAME --force
aws cloudformation delete-stack --stack-name sam-cron
```

---

## CICD

TBD

---

## Resources

- [aws sam hello-world](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-getting-started-hello-world.html)
- [sam lambda local invocation with --env-vars](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-using-invoke.html)
- [layers/dependencies in aws sam](https://aws.amazon.com/blogs/compute/working-with-aws-lambda-and-lambda-layers-in-aws-sam/)
